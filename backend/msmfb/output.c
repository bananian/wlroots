#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <wlr/interfaces/wlr_output.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/util/log.h>
#include "backend/msmfb.h"
#include "util/signal.h"

static struct wlr_msmfb_output *msmfb_output_from_output(
		struct wlr_output *wlr_output) {
	assert(wlr_output_is_msmfb(wlr_output));
	return (struct wlr_msmfb_output *)wlr_output;
}

#define ION_DEVICE "/dev/ion"

static bool allocate_ion(struct wlr_msmfb_output *output)
{
	struct ion_fd_data ion_fd;
	struct ion_allocation_data ion_alloc;
	output->iondev = open(ION_DEVICE, O_RDWR);
	if(output->iondev < 0){
		wlr_log(WLR_ERROR, "Failed to open /dev/ion: %s",
				strerror(errno));
		return false;
	}
	ion_alloc.flags = 0;
	ion_alloc.len = output->memlen;
	ion_alloc.align = sysconf(_SC_PAGESIZE);
	ion_alloc.heap_id_mask = ION_HEAP(ION_IOMMU_HEAP_ID) |
		ION_HEAP(ION_SYSTEM_CONTIG_HEAP_ID);
	if(ioctl(output->iondev, ION_IOC_ALLOC, &ion_alloc) < 0){
		wlr_log(WLR_ERROR,
			"Failed to allocate ION memory for framebuffer: %s",
			strerror(errno));
		close(output->iondev);
		return false;
	}
	output->ionh = ion_fd.handle = ion_alloc.handle;
	if(ioctl(output->iondev, ION_IOC_MAP, &ion_fd) < 0){
		struct ion_handle_data handle = {
			.handle = ion_fd.handle
		};
		wlr_log(WLR_ERROR, "Failed to map ION buffer: ION_IOC_MAP: %s",
				strerror(errno));
		ioctl(output->iondev, ION_IOC_FREE, &handle);
		close(output->iondev);
		return false;
	}
	output->ionfd = ion_fd.fd;
	output->mem = mmap(NULL, output->memlen, PROT_READ | PROT_WRITE,
			MAP_SHARED, ion_fd.fd, 0);
	if(output->mem == MAP_FAILED){
		struct ion_handle_data handle = {
			.handle = ion_fd.handle
		};
		wlr_log(WLR_ERROR, "Failed to map ION buffer: mmap: %s",
				strerror(errno));
		ioctl(output->iondev, ION_IOC_FREE, &handle);
		close(output->iondev);
		return false;
	}
	return true;
}

static void deallocate_ion(struct wlr_msmfb_output *output)
{
	struct ion_handle_data handle = {
		.handle = output->ionh
	};
	if(output->iondev < 0) return;
	if(munmap(output->mem, output->memlen) < 0){
		wlr_log(WLR_ERROR, "Failed to unmap ION buffer: munmap: %s",
				strerror(errno));
	}
	if(ioctl(output->iondev, ION_IOC_FREE, &handle) < 0){
		wlr_log(WLR_ERROR, "Failed to free ION buffer: %s",
				strerror(errno));
	}
}

static bool create_fbo(struct wlr_msmfb_output *output,
		unsigned int width, unsigned int height) {
	output->overlay.src.width = width;
	output->overlay.src.height = height;
	output->overlay.src.format = MDP_RGB_565;
	output->overlay.src_rect.w = width;
	output->overlay.src_rect.h = height;
	output->overlay.dst_rect.w = width;
	output->overlay.dst_rect.h = height;
	output->overlay.alpha = 0xFF;
	output->overlay.transp_mask = MDP_TRANSP_NOP;
	output->overlay.id = MSMFB_NEW_REQUEST;

	if(ioctl(output->fbdev, MSMFB_OVERLAY_SET, &output->overlay) < 0){
		wlr_log(WLR_DEBUG,
			"Failed to set overlay, probably not an MSMFB: %s",
			strerror(errno));
		return false;
	}

	output->memlen = width * height * 2; /* 16 bits per pixel */
	if(!allocate_ion(output)){
		return false;
	}
	memset(output->mem, 255, output->memlen); /* Should be all white */

	if (!wlr_egl_make_current(output->backend->egl, EGL_NO_SURFACE, NULL)) {
		return false;
	}

	GLuint rbo;
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, output->backend->internal_format,
		width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	GLuint fbo;
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_RENDERBUFFER, rbo);
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	wlr_egl_unset_current(output->backend->egl);

	if (status != GL_FRAMEBUFFER_COMPLETE) {
		wlr_log(WLR_ERROR, "Failed to create FBO");
		return false;
	}

	output->fbo = fbo;
	output->rbo = rbo;
	return true;
}

static void destroy_fbo(struct wlr_msmfb_output *output) {
	if (!wlr_egl_make_current(output->backend->egl, EGL_NO_SURFACE, NULL)) {
		return;
	}

	glDeleteFramebuffers(1, &output->fbo);
	glDeleteRenderbuffers(1, &output->rbo);

	wlr_egl_unset_current(output->backend->egl);
	if(output->mem) deallocate_ion(output);

	output->fbo = 0;
	output->rbo = 0;
}

static bool output_set_custom_mode(struct wlr_output *wlr_output, int32_t width,
		int32_t height, int32_t refresh) {
	struct wlr_msmfb_output *output =
		msmfb_output_from_output(wlr_output);

	if (refresh <= 0) {
		refresh = MSMFB_DEFAULT_REFRESH;
	}

	destroy_fbo(output);
	if (!create_fbo(output, width, height)) {
		wlr_output_destroy(wlr_output);
		return false;
	}

	output->frame_delay = 1000000 / refresh;

	wlr_output_update_custom_mode(&output->wlr_output, width, height, refresh);
	return true;
}

static bool output_attach_render(struct wlr_output *wlr_output,
		int *buffer_age) {
	struct wlr_msmfb_output *output =
		msmfb_output_from_output(wlr_output);

	if (!wlr_egl_make_current(output->backend->egl, EGL_NO_SURFACE, NULL)) {
		return false;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, output->fbo);

	if (buffer_age != NULL) {
		// HACK: set to 1 instead of 0 to avoid CPU burn
		// https://github.com/swaywm/wlroots/pull/2410#discussion_r497615821
		*buffer_age = 1;
	}
	return true;
}

static bool output_enable(struct wlr_msmfb_output *output) {
	if(ioctl(output->fbdev, FBIOBLANK, FB_BLANK_UNBLANK) < 0) {
		wlr_log(WLR_DEBUG,
			"Failed to unblank display: %s",
			strerror(errno));
	}
	if(ioctl(output->fbdev, MSMFB_OVERLAY_SET, &output->overlay) < 0) {
		wlr_log(WLR_DEBUG,
			"Failed to set overlay, probably not an MSMFB: %s",
			strerror(errno));
		return false;
	}
	return true;
}

static bool output_disable(struct wlr_msmfb_output *output) {
	if(ioctl(output->fbdev, MSMFB_OVERLAY_UNSET, &output->overlay.id) < 0) {
		wlr_log(WLR_DEBUG,
			"Failed to unset overlay: %s",
			strerror(errno));
	}
	if(ioctl(output->fbdev, FBIOBLANK, FB_BLANK_POWERDOWN) < 0) {
		wlr_log(WLR_DEBUG,
			"Failed to blank display: %s",
			strerror(errno));
	}
	output->overlay.id = MSMFB_NEW_REQUEST;
	return true;
}

static bool output_test(struct wlr_output *wlr_output) {
	if (wlr_output->pending.committed & WLR_OUTPUT_STATE_MODE) {
		assert(wlr_output->pending.mode_type == WLR_OUTPUT_STATE_MODE_CUSTOM);
	}

	return true;
}

static bool output_commit(struct wlr_output *wlr_output) {
	struct wlr_msmfb_output *output =
		msmfb_output_from_output(wlr_output);

	if (!output_test(wlr_output)) {
		return false;
	}

	if (wlr_output->pending.committed & WLR_OUTPUT_STATE_ENABLED){
		if(wlr_output->pending.enabled && !wlr_output->enabled){
			if(!output_enable(output)) return false;
			wl_event_source_timer_update(output->frame_timer,
					output->frame_delay);
		}
		if(!wlr_output->pending.enabled && wlr_output->enabled){
			if(!output_disable(output)) return false;
		}
		wlr_output->enabled = wlr_output->pending.enabled;
	}

	if (wlr_output->pending.committed & WLR_OUTPUT_STATE_MODE) {
		if (!output_set_custom_mode(wlr_output,
				wlr_output->pending.custom_mode.width,
				wlr_output->pending.custom_mode.height,
				wlr_output->pending.custom_mode.refresh)) {
			return false;
		}
	}

	if (wlr_output->pending.committed & WLR_OUTPUT_STATE_BUFFER) {
		pixman_box32_t box = wlr_output->pending.damage.extents;
		int32_t box_width = box.x2 - box.x1;
		if(box_width){
			GLint format, type;
			glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_FORMAT,
					&format);
			glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_TYPE,
					&type);
			for(int y = box.y1; y < box.y2; y++){
				glReadPixels(box.x1, output->vsi.yres - y - 1,
					box_width, 1, format, type,
					output->mem +
					(y * output->vsi.xres + box.x1) * 2);
			}
			struct msmfb_overlay_data ovd = {
				.data = {
					.flags = 0,
					.offset = 0,
					.memory_id = output->ionfd
				},
				.id = output->overlay.id
			};
			if(ioctl(output->fbdev, MSMFB_OVERLAY_PLAY, &ovd) < 0){
				wlr_log(WLR_ERROR,
					"Failed to play overlay: %s",
					strerror(errno));
			}
			struct mdp_display_commit cm = {
				.flags = MDP_DISPLAY_COMMIT_OVERLAY,
				.wait_for_finish = 0
			};
			if(ioctl(output->fbdev, MSMFB_DISPLAY_COMMIT, &cm) < 0){
				wlr_log(WLR_ERROR,
					"Failed commit overlay to display: %s",
					strerror(errno));
			}
			/*wlr_log(WLR_DEBUG, "Showed frame %d,%d+%d+%d",
				box.x1, box.y1, box_width, box.y2 - box.y1);*/
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		wlr_egl_unset_current(output->backend->egl);
		wlr_output_send_present(wlr_output, NULL);
		wl_event_source_timer_update(output->frame_timer,
				output->frame_delay);
	}

	return true;
}

static void output_rollback_render(struct wlr_output *wlr_output) {
	struct wlr_msmfb_output *output =
		msmfb_output_from_output(wlr_output);
	assert(wlr_egl_is_current(output->backend->egl));
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	wlr_egl_unset_current(output->backend->egl);
}

static void output_destroy(struct wlr_output *wlr_output) {
	struct wlr_msmfb_output *output =
		msmfb_output_from_output(wlr_output);
	wl_list_remove(&output->link);
	wl_event_source_remove(output->frame_timer);
	destroy_fbo(output);
	free(output);
}

static const struct wlr_output_impl output_impl = {
	.destroy = output_destroy,
	.attach_render = output_attach_render,
	.commit = output_commit,
	.rollback_render = output_rollback_render,
};

bool wlr_output_is_msmfb(struct wlr_output *wlr_output) {
	return wlr_output->impl == &output_impl;
}

static int signal_frame(void *data) {
	struct wlr_msmfb_output *output = data;
	if(output->wlr_output.enabled){
		wlr_output_send_frame(&output->wlr_output);
		wl_event_source_timer_update(output->frame_timer,
				output->frame_delay);
	}
	return 0;
}

struct wlr_output *wlr_msmfb_add_output(struct wlr_backend *wlr_backend,
		const char *fbdev)
{
	struct wlr_msmfb_backend *backend =
		msmfb_backend_from_backend(wlr_backend);

	struct wlr_msmfb_output *output =
		calloc(1, sizeof(struct wlr_msmfb_output));
	if (output == NULL) {
		wlr_log(WLR_ERROR, "Failed to allocate wlr_msmfb_output");
		return NULL;
	}
	output->iondev = -1;
	output->backend = backend;
	wlr_output_init(&output->wlr_output, &backend->backend, &output_impl,
		backend->display);
	struct wlr_output *wlr_output = &output->wlr_output;

	output->fbdev = open(fbdev, O_RDWR|O_CLOEXEC);
	if (output->fbdev < 0) {
		wlr_log(WLR_DEBUG, "msmfb %s failed to open: %s",
			fbdev, strerror(errno));
		wlr_output_destroy(&output->wlr_output);
		return NULL;
	}

	if (ioctl(output->fbdev, FBIOGET_FSCREENINFO, &output->fsi)) {
		wlr_log(WLR_ERROR, "Getting fscreeninfo on %s failed: %s",
			fbdev, strerror(errno));
		goto error;
	}
	if (ioctl(output->fbdev, FBIOGET_VSCREENINFO, &output->vsi)) {
		wlr_log(WLR_ERROR, "Getting vscreeninfo on %s failed: %s",
			fbdev, strerror(errno));
		goto error;
	}

	if (!create_fbo(output, output->vsi.xres, output->vsi.yres)) {
		goto error;
	}

	output_set_custom_mode(wlr_output,
			output->vsi.xres, output->vsi.yres, 0);
	strncpy(wlr_output->make, "msmfb", sizeof(wlr_output->make));
	strncpy(wlr_output->model, "msmfb", sizeof(wlr_output->model));
	snprintf(wlr_output->name, sizeof(wlr_output->name), "MSM-%zd",
		++backend->last_output_num);

	char description[128];
	snprintf(description, sizeof(description),
		"MSM framebuffer on %s", fbdev);
	wlr_output_set_description(wlr_output, description);

	if (!output_attach_render(wlr_output, NULL)) {
		goto error;
	}

	wlr_renderer_begin(backend->renderer, wlr_output->width, wlr_output->height);
	wlr_renderer_clear(backend->renderer, (float[]){ 1.0, 1.0, 1.0, 1.0 });
	wlr_renderer_end(backend->renderer);

	struct wl_event_loop *ev = wl_display_get_event_loop(backend->display);
	output->frame_timer = wl_event_loop_add_timer(ev, signal_frame, output);

	wl_list_insert(&backend->outputs, &output->link);

	if (backend->started) {
		wl_event_source_timer_update(output->frame_timer, output->frame_delay);
		wlr_output_update_enabled(wlr_output, true);
		wlr_signal_emit_safe(&backend->backend.events.new_output, wlr_output);
	}

	return wlr_output;

error:
	close(output->fbdev);
	wlr_output_destroy(&output->wlr_output);
	return NULL;
}
