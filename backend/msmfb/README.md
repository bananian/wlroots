# MSMFB backend

This is a wlroots backend for Qualcomm framebuffer devices.
Qualcomm framebuffers are represented as normal `/dev/fbX` devices, but they
have a proprietary API (with a compatibility layer which does not work
properly on all devices).

## Credits
This backend is based on the `headless` backend and was inspired by the
[`fbdev` backend](https://github.com/ollieparanoid/wlroots/blob/fbdev/backend/fbdev/output.c).
Additional copyrights:
```
Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
```
