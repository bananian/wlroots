/*
 * This an unstable interface of wlroots. No guarantees are made regarding the
 * future consistency of this API.
 */
#ifndef WLR_USE_UNSTABLE
#error "Add -DWLR_USE_UNSTABLE to enable unstable wlroots features"
#endif

#ifndef WLR_BACKEND_MSMFB_H
#define WLR_BACKEND_MSMFB_H

#include <wlr/backend.h>
#include <wlr/types/wlr_output.h>

/**
 * Creates a msmfb backend. A msmfb backend has no outputs or inputs by
 * default.
 */
struct wlr_backend *wlr_msmfb_backend_create(struct wl_display *display,
	wlr_renderer_create_func_t create_renderer_func);
/**
 * Create a new msmfb output from a framebuffer device.
 */
struct wlr_output *wlr_msmfb_add_output(struct wlr_backend *backend,
	const char *fbdev);
bool wlr_backend_is_msmfb(struct wlr_backend *backend);
bool wlr_output_is_msmfb(struct wlr_output *output);

#endif
