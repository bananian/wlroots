#ifndef BACKEND_MSMFB_H
#define BACKEND_MSMFB_H

#include <linux/fb.h>
#include "backend/msmfb/msm_ion.h"
#include "backend/msmfb/msm_mdp.h"
#include <wlr/backend/msmfb.h>
#include <wlr/backend/interface.h>
#include <wlr/render/gles2.h>

#define MSMFB_DEFAULT_REFRESH (30 * 1000) // 30 Hz

struct wlr_msmfb_backend {
	struct wlr_backend backend;
	struct wlr_egl priv_egl; // may be uninitialized
	struct wlr_egl *egl;
	struct wlr_renderer *renderer;
	struct wl_display *display;
	struct wl_list outputs;
	size_t last_output_num;
	struct wl_listener display_destroy;
	struct wl_listener renderer_destroy;
	bool started;
	GLenum internal_format;
};

struct wlr_msmfb_output {
	struct wlr_output wlr_output;

	struct wlr_msmfb_backend *backend;
	struct wl_list link;

	GLuint fbo, rbo;

	int fbdev, iondev, ionfd;
	ion_user_handle_t ionh;
	unsigned char *mem;
	size_t memlen;

	struct mdp_overlay overlay;

	struct fb_fix_screeninfo fsi;
	struct fb_var_screeninfo vsi;

	struct wl_event_source *frame_timer;
	int frame_delay; // ms
};

struct wlr_msmfb_backend *msmfb_backend_from_backend(
	struct wlr_backend *wlr_backend);

#endif
